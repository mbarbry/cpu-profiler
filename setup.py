from distutils.core import setup
import setuptools

setup(
    name='cpu-profiler',
    description='A module for monitoring cpu frequency',
    long_description=open('README.md').read(),
    version='0.2.1',
    author='Marc Barbry',
    author_email='marc.barbry@mailoo.org',
    url='https://gitlab.com/mbarbry/cpu-profiler',
    scripts=['cpuprof'],
    license='MIT'
)
