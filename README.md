Description
===========

Python module to monitor the CPU frequency while running an application.
The actual version is based on the phylosophy of the memory_profiler from https://github.com/fabianp/memory_profiler
but apply to CPU frequency.
The cpu-profiler uses the command ps to read the percentage of cpu running and memory comsumtion
or read the cpu frequency of each cores in the /proc/cpuinfo file.

Requirements
============

To install cpu-profiler you need python with [matplotlib](http://matplotlib.org/) and
[numpy](http://www.numpy.org/) installed.

Then you just need to download the code, go to the source directory and run,
  
    python setup.py install


To run the program,
* and get the percentage of cpu and memory comsumption: 

    cpuprof top < executable >
* and get the frequency of each core: 

    cpuprof cpufreq: < executable >

plot the graphs,

    cpuprof plot
option --plt_kind allow to plot data from top or cpufreq


Some figures
============

## Figure from cpufreq command
![Alt text](pictures/cpufreq.png)

## Figure from top command
![Alt text](pictures/top.png)
